using System;
using Unity.Robotics.ROSTCPConnector;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.XR.ARFoundation;

namespace UI_Toolkit.Scripts
{
    public class Settings : MonoBehaviour
    {
        private const string PlayerPrefsKeyConnectOnStart = "ConnectOnStart";
        private const string PlayerPrefsKeyTableVisibility = "TableVisibility";
        private const string PlayerPrefsKeyARCursorVisibility = "ARCursorVisibility";
        private const string PlayerPrefsKeyARPlaneVisibility = "ARPlaneVisibility";

        private Button settingsButton;
        private VisualElement content;
        private TextField ipTextField;
        private TextField portTextField;
        private Button saveButton;
        private Toggle connectOnStartToggle;
        private Toggle connectToggle;
        private Toggle tableToggle;
        private Toggle arCursorToggle;
        private Toggle arPlaneToggle;

        [SerializeField]
        private ROSConnection connection;
        [SerializeField]
        private MeshRenderer table;
        [SerializeField]
        private ARCursor arCursor;
        [SerializeField]
        private ARPlaneManager arPlaneManager;

        private string rosIPAddress;
        private int rosPort;
        private bool shouldConnectOnStart;
        private bool shouldReconnect;
        private bool shouldShowTable;
        private bool shouldShowARCursor;
        private bool shouldShowARPlane;

        // Start is called before the first frame update
        void Start()
        {
            var root = GetComponent<UIDocument>().rootVisualElement;

            content = root.Q<VisualElement>("Content");

            ipTextField = root.Q<TextField>("IP");
            ipTextField.RegisterValueChangedCallback(evt => ChangeIP(evt.newValue));
            
            portTextField = root.Q<TextField>("Port");
            portTextField.RegisterValueChangedCallback(evt => ChangePort(evt.newValue));
            
            connectOnStartToggle = root.Q<Toggle>("ConnectOnStart");
            connectOnStartToggle.RegisterValueChangedCallback(evt => ConnectOnStart(evt.newValue));
            
            connectToggle = root.Q<Toggle>("Connect");
            connectToggle.RegisterValueChangedCallback(evt => Connect(evt.newValue));
            
            tableToggle = root.Q<Toggle>("TableVisibility");
            tableToggle.RegisterValueChangedCallback(evt => ShowTable(evt.newValue));

            arCursorToggle = root.Q<Toggle>("ARCursorVisibility");
            arCursorToggle.RegisterValueChangedCallback(evt => ShowARCursor(evt.newValue));

            arPlaneToggle = root.Q<Toggle>("ARPlaneVisibility");
            arPlaneToggle.RegisterValueChangedCallback(evt => ShowARPlane(evt.newValue));

            saveButton = root.Q<Button>("SaveButton");
            saveButton.clicked += ApplySettings;

            settingsButton = root.Q<Button>("SettingsButton");
            settingsButton.clicked += OnSettingsButtonClicked;

            LoadSettings();

            if(Application.internetReachability == NetworkReachability.NotReachable)
            {
                Debug.Log("Error. Check internet connection!");
            }
            
            ApplySettings();
        }

        private void OnSettingsButtonClicked()
        {
            content.visible = !content.visible;
        }

        private void ChangeIP(string ip)
        {
            rosIPAddress = ip;
            if (rosIPAddress != connection.RosIPAddress)
            {
                connection.RosIPAddress = rosIPAddress;
                shouldReconnect = shouldConnectOnStart;
            }
            
        }
        
        private void ChangePort(string port)
        {
            rosPort = Convert.ToInt32(port);
            if (rosPort != connection.RosPort)
            {
                connection.RosPort = rosPort;
                shouldReconnect = shouldConnectOnStart;
            }
        }

        private void Connect(bool connect)
        {
            if (connect)
            {
                connection.Connect();
                shouldReconnect = false;
            }
            else
                connection.Disconnect();
        }
        
        private void ShowTable(bool visible)
        {
            table.enabled = visible;
            arCursor.hasTable = visible;
            shouldShowTable = visible;
        }

        private void ShowARCursor(bool visible)
        {
            arCursor.ShowCursor(visible);
            shouldShowARCursor = visible;
        }

        private void ShowARPlane(bool visible)
        {
            if(arPlaneManager.planePrefab!=null)
            {
                arPlaneManager.planePrefab.GetComponent<ARPlaneMeshVisualizer>().enabled = visible;
                foreach (var plane in GameObject.FindGameObjectsWithTag("ARPlane"))
                    plane.GetComponent<ARPlaneMeshVisualizer>().enabled = visible;
            }
            shouldShowARPlane = visible;
        }

        private void ConnectOnStart(bool connect)
        {
            shouldConnectOnStart = connect;
            if (shouldConnectOnStart != connection.ConnectOnStart)
                connection.ConnectOnStart = shouldConnectOnStart;
            shouldReconnect = shouldConnectOnStart;
        }

        private void ApplySettings()
        {
            if (shouldReconnect)
            {
                connection.Disconnect();
                connection.Connect();
                connectToggle.SetValueWithoutNotify(true);
            }

            ShowTable(shouldShowTable);
            ShowARCursor(shouldShowARCursor);
            ShowARPlane(shouldShowARPlane);

            SaveSettings();

            content.visible = false;
        }
        
        private void SaveSettings()
        {
            ROSConnection.SetIPPref(rosIPAddress);
            ROSConnection.SetPortPref(rosPort);
            PlayerPrefs.SetInt(PlayerPrefsKeyConnectOnStart, shouldConnectOnStart? 1 : 0);
            PlayerPrefs.SetInt(PlayerPrefsKeyTableVisibility, shouldShowTable? 1 : 0);
            PlayerPrefs.SetInt(PlayerPrefsKeyARCursorVisibility, shouldShowARCursor ? 1 : 0);
            PlayerPrefs.SetInt(PlayerPrefsKeyARPlaneVisibility, shouldShowARPlane ? 1 : 0);

            // save settings
            PlayerPrefs.Save();
        }
 
        private void LoadSettings()
        {
            rosIPAddress = ROSConnection.RosIPAddressPref;
            rosPort = ROSConnection.RosPortPref;
            shouldConnectOnStart = PlayerPrefs.GetInt(PlayerPrefsKeyConnectOnStart, 1) == 1;
            shouldShowTable = PlayerPrefs.GetInt(PlayerPrefsKeyTableVisibility, 1) == 1;
            shouldShowARCursor = PlayerPrefs.GetInt(PlayerPrefsKeyARCursorVisibility, 1) == 1;
            shouldShowARPlane = PlayerPrefs.GetInt(PlayerPrefsKeyARPlaneVisibility, 1) == 1;

            // set values to UI
            ipTextField.value = rosIPAddress;
            portTextField.value = rosPort.ToString();
            connectOnStartToggle.value = shouldConnectOnStart;
            tableToggle.value = shouldShowTable;
            arCursorToggle.value = shouldShowARCursor;
            arPlaneToggle.value = shouldShowARPlane;
        }
        
    }
}
