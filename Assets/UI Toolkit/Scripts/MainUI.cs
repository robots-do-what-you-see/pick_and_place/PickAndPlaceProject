using UnityEngine;
using UnityEngine.UIElements;

namespace UI_Toolkit.Scripts
{
    public class MainUI : MonoBehaviour
    {
        private Button placeButton;
        private Button saveButton;
        private Button loadButton;
        private Button nextButton;
        private Button previousButton;
        private Label feedbackLabel;

        [SerializeField]
        private ARCursor arCursor;

        [SerializeField]
        private TrajectoryPlanner trajectoryPlanner;

        [SerializeField]
        private SourceDestinationPublisher destinationPublisher;

        // Start is called before the first frame update
        void Start()
        {
            var root = GetComponent<UIDocument>().rootVisualElement;

            placeButton = root.Q<Button>("PlaceButton");
            placeButton.clicked += arCursor.PlaceRobot;

            saveButton = root.Q<Button>("SaveButton");
            saveButton.clicked += arCursor.SaveAnchor;

            loadButton = root.Q<Button>("LoadButton");
            loadButton.clicked += arCursor.LoadAnchor;

            nextButton = root.Q<Button>("NextButton");
            previousButton = root.Q<Button>("PreviousButton");

            nextButton.clicked += trajectoryPlanner.PublishNextTask;
            previousButton.clicked += trajectoryPlanner.PublishPreviousTask;

            feedbackLabel = root.Q<Label>("FeedbackLabel");
            feedbackLabel.text = "";

            arCursor.UpdateFeedbackText += (text) => feedbackLabel.text = text;
        }
        
    }
}
