﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using Microsoft.Azure.SpatialAnchors;
using Microsoft.Azure.SpatialAnchors.Unity;
using Microsoft.Azure.SpatialAnchors.Unity.Examples;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.XR.ARFoundation;

public class ARCursor : MonoBehaviour
{
    [SerializeField]
    GameObject cursorChildObject;
    [SerializeField]
    GameObject objectToPlace;
    [SerializeField]
    ARRaycastManager raycastManager;

    [SerializeField]
    GameObject rosAnchor;

    [SerializeField]
    Vector3 rosAnchorOffset = new Vector3(0.0f, -0.64f, 0.0f);

    [SerializeField]
    private GameObject cursor;
    public bool useCursor = true;
    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    public bool hasTable { set; get; } = true;

    public AnchorExchanger anchorExchanger = new AnchorExchanger();

    /// <summary>
    /// Gets the <see cref="SpatialAnchorManager"/> instance used by this demo.
    /// </summary>
    public SpatialAnchorManager CloudManager { get { return spatialAnchorManager; } }

    [SerializeField]
    [Tooltip("SpatialAnchorManager instance to use for this demo. This is required.")]
    private SpatialAnchorManager spatialAnchorManager = null;

    protected readonly List<string> anchorIdsToLocate = new List<string>();
    private int anchorsLocated = 0;
    private int anchorsExpected = 0;
    private string _anchorKeyToFind = null;
    private long? _anchorNumberToFind;
    protected AnchorLocateCriteria anchorLocateCriteria = null;
    protected CloudSpatialAnchor currentCloudAnchor;
    protected CloudNativeAnchor currentNativeAnchor;
    protected CloudSpatialAnchorWatcher currentWatcher;
    protected bool isErrorActive = false;

    /// <summary>
    /// Gets or sets the base URL for the example sharing service.
    /// </summary>
    public string BaseSharingUrl { get; set; }

    public delegate void UpdateFeedbackTextDelegate(string text);
    public event UpdateFeedbackTextDelegate UpdateFeedbackText;


    // Start is called before the first frame update
    void Start()
    {
        cursorChildObject.SetActive(useCursor);

        ConfigureSpatialAnchorService();
    }

    // Update is called once per frame
    void Update()
    {
        if (useCursor)
        {
            UpdateCursor();
        }
        
        if (Input.touchCount == 0)
            return;

        var touch = Input.GetTouch(0);
        
        if (touch.phase != TouchPhase.Began)
            return;

        
        if (useCursor && objectToPlace!=null)
        {
            GameObject.Instantiate(objectToPlace, transform.position, transform.rotation);
        }
        else if (objectToPlace != null)
        {
            raycastManager.Raycast(touch.position, s_Hits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon);
            if (s_Hits.Count > 0)
            {
                GameObject.Instantiate(objectToPlace, s_Hits[0].pose.position, s_Hits[0].pose.rotation);
            }
        }

        
    }

    /// <summary>
    /// Display Text in duration
    /// </summary>
    /// <param name="text"></param>
    /// <param name="duration">display time in ms; 0: no reset</param>
    private async void UpdateFeedbackTextAsync(string text, int duration = 3000)
    {
        UpdateFeedbackText(text);
        if (duration > 0)
        {
            await Task.Delay(duration);
            UpdateFeedbackText("");
        }
    }

    /// <summary>
    /// Setup Sharing Service BaseURL and callback for Anchor Status Event.
    /// </summary>
    private void ConfigureSpatialAnchorService()
    {
        var sharingConfig = Resources.Load<SpatialAnchorSharingConfig>("SpatialAnchorSharingConfig");
        if (string.IsNullOrWhiteSpace(BaseSharingUrl) && sharingConfig != null)
        {
            BaseSharingUrl = sharingConfig.BaseSharingURL;
        }

        if (string.IsNullOrEmpty(BaseSharingUrl))
        {
            Debug.LogError($"Need to set {nameof(BaseSharingUrl)}");
        }
        anchorExchanger.BaseAddress = BaseSharingUrl;

        CloudManager.SessionUpdated += CloudManager_SessionUpdated;
        CloudManager.AnchorLocated += CloudManager_AnchorLocated;
        CloudManager.LocateAnchorsCompleted += CloudManager_LocateAnchorsCompleted;
        CloudManager.LogDebug += CloudManager_LogDebug;
        CloudManager.Error += CloudManager_Error;

        anchorLocateCriteria = new AnchorLocateCriteria();

        if (!rosAnchor.TryGetComponent(out currentNativeAnchor))
        {
            currentNativeAnchor = rosAnchor.AddComponent<CloudNativeAnchor>();
        }
    }

    /// <summary>
    /// Called when the Anchor is found
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    private void CloudManager_AnchorLocated(object sender, AnchorLocatedEventArgs args)
    {
        switch (args.Status)
        {
            case LocateAnchorStatus.AlreadyTracked:
                {
                    Debug.Log("Anchor Already Tracked");
                    UpdateFeedbackTextAsync("Anchor Already Tracked");

                    PlaceRobotWithAnchor();

                    CloudManager.StopSession();
                    break;
                }
                
            case LocateAnchorStatus.Located:
                {
                    Debug.Log("Anchor Located");
                    UpdateFeedbackTextAsync("Anchor Located");
                    currentCloudAnchor = args.Anchor;

                    PlaceRobotWithAnchor();

                    CloudManager.StopSession();
                    break;
                }
            case LocateAnchorStatus.NotLocatedAnchorDoesNotExist:
                {
                    Debug.Log("No avaibale Anchor");
                    UpdateFeedbackTextAsync("No avaibale Anchor");
                    CloudManager.StopSession();
                    break;
                }
        }
    }

    private void CloudManager_LocateAnchorsCompleted(object sender, LocateAnchorsCompletedEventArgs args)
    {
        Debug.Log("Locate pass complete");
    }

    private void CloudManager_SessionUpdated(object sender, SessionUpdatedEventArgs args)
    {
        Debug.Log("Session Updated");
    }

    private void CloudManager_Error(object sender, SessionErrorEventArgs args)
    {
        isErrorActive = true;
        Debug.Log($"Error: {args.ErrorMessage}");
    }

    private void CloudManager_LogDebug(object sender, OnLogDebugEventArgs args)
    {
        //Debug.Log($"LogDebug: {args.Message}");
    }

    public void PlaceRobot()
    {
        var position = transform.position;
        var rotation = transform.rotation;
        if (!hasTable)
            position = position + rosAnchorOffset;

        PlaceRobot(position, rotation);

        if (currentNativeAnchor != null)
            currentNativeAnchor.SetPose(position, rotation);
    }

    private void PlaceRobotWithAnchor()
    {
        var anchorPose = currentCloudAnchor.GetPose();
        PlaceRobot(anchorPose.position, anchorPose.rotation);
    }

    private void PlaceRobot(Vector3 position, Quaternion rotation)
    {
        rosAnchor.SetActive(false);
        rosAnchor.transform.SetPositionAndRotation(position, rotation);
        rosAnchor.SetActive(true);
    }

    /// <summary>
    /// Saves the current anchor to the cloud.
    /// </summary>
    public async void SaveAnchor()
    {
        if (string.IsNullOrEmpty(BaseSharingUrl))
            ConfigureSpatialAnchorService();

        _anchorKeyToFind = null;

        ConfigureSession();

        await CloudManager.StartSessionAsync();

        await SaveCurrentAnchorToCloudAsync();

        CloudManager.StopSession();

        await CloudManager.ResetSessionAsync();
    }

    /// <summary>
    /// Load anchor from the cloud
    /// </summary>
    public async void LoadAnchor()
    {
        if (string.IsNullOrEmpty(BaseSharingUrl))
            ConfigureSpatialAnchorService();

        _anchorKeyToFind = await anchorExchanger.RetrieveLastAnchorKey();

        if (string.IsNullOrEmpty(_anchorKeyToFind))
        {
            UpdateFeedbackTextAsync("No avaibale Anchor");
            return;
        }
        else
            UpdateFeedbackTextAsync($"Loading Anchor {_anchorKeyToFind}");

        ConfigureSession();

        await CloudManager.StartSessionAsync();
        currentWatcher = CreateWatcher();
    }

    private void ConfigureSession()
    {
        // for debug text in the main menu
        UpdateFeedbackText("");

        // stop session as use may cancel current sessinon.
        if (CloudManager.IsSessionStarted)
            CloudManager.StopSession();

        List<string> anchorsToFind = new List<string>();

        if(_anchorKeyToFind != null)
            anchorsToFind.Add(_anchorKeyToFind);
        
        anchorsExpected = anchorsToFind.Count;
        SetAnchorIdsToLocate(anchorsToFind);
    }

    /// <summary>
    /// Saves the current anchor to the cloud.
    /// </summary>
    protected async Task SaveCurrentAnchorToCloudAsync()
    {
        // Get the cloud-native anchor behavior
        var cna = currentNativeAnchor;
        if(cna == null)
        {
            Debug.Log("No CloudNativeAnchor");
            return;
        }

        // If the cloud portion of the anchor hasn't been created yet, create it
        if (cna.CloudAnchor == null)
        {
            await cna.NativeToCloud();
            // set the anchor to expire automatically in a week
            cna.CloudAnchor.Expiration = DateTimeOffset.Now.AddDays(7);
        }
        else
        {
            await cna.NativeToCloud();
        }

        // Get the cloud portion of the anchor
        var cloudAnchor = cna.CloudAnchor;

        if (CloudManager == null)
        {
            Debug.Log("No CloudManager");
            return;
        }

        while (!CloudManager.IsReadyForCreate)
        {
            await Task.Delay(330);
            float createProgress = CloudManager.SessionStatus.RecommendedForCreateProgress;
            // for debug text in the main menu
            UpdateFeedbackText($"Move your device to capture more environment data: {createProgress:0%}");
        }

        // for debug text in the main menu
        UpdateFeedbackText("Saving...");

        try
        {
            // Actually save
            await CloudManager.CreateAnchorAsync(cloudAnchor);

            // Store
            currentCloudAnchor = cloudAnchor;

            // Success?
            if ((currentCloudAnchor != null) && !isErrorActive)
            {
                // Await override, which may perform additional tasks
                // such as storing the key in the AnchorExchanger
                await OnSaveCloudAnchorSuccessfulAsync();
            }
            else
            {
                OnSaveCloudAnchorFailed(new Exception("Failed to save, but no exception was thrown."));
            }
        }
        catch (Exception ex)
        {
            OnSaveCloudAnchorFailed(ex);
        }
    }

    /// <summary>
    /// Called when a cloud anchor is saved successfully.
    /// </summary>
    protected async Task OnSaveCloudAnchorSuccessfulAsync()
    {
        var anchorNumber = await anchorExchanger.StoreAnchorKey(currentCloudAnchor.Identifier);
        var text = $"Created anchor {anchorNumber}:{currentCloudAnchor.Identifier}.";
        Debug.Log(text);
        UpdateFeedbackTextAsync(text);
    }

    /// <summary>
    /// Called when a cloud anchor is not saved successfully.
    /// </summary>
    /// <param name="exception">The exception.</param>
    protected virtual void OnSaveCloudAnchorFailed(Exception exception)
    {
        // we will block the next step to show the exception message in the UI.
        isErrorActive = true;
        Debug.LogException(exception);
        var text = $"Failed to save anchor {exception.Message}"; 
        // for debug text in the main menu
        UpdateFeedbackTextAsync(text);
    }

    protected void SetAnchorIdsToLocate(IEnumerable<string> anchorIds)
    {
        if (anchorIds == null)
        {
            throw new ArgumentNullException(nameof(anchorIds));
        }

        anchorLocateCriteria.NearAnchor = new NearAnchorCriteria();

        anchorIdsToLocate.Clear();
        anchorIdsToLocate.AddRange(anchorIds);

        anchorLocateCriteria.Identifiers = anchorIdsToLocate.ToArray();
    }

    protected CloudSpatialAnchorWatcher CreateWatcher()
    {
        if ((CloudManager != null) && (CloudManager.Session != null))
        {
            return CloudManager.Session.CreateWatcher(anchorLocateCriteria);
        }
        else
        {
            return null;
        }
    }

    public void ShowCursor(bool visible)
    {
        cursor.SetActive(visible);
    }

    void UpdateCursor()
    {
        var screenPosition = Camera.main.ViewportToScreenPoint(new Vector2(0.5f, 0.5f));

        raycastManager.Raycast(screenPosition, s_Hits, UnityEngine.XR.ARSubsystems.TrackableType.Planes);
        if (s_Hits.Count > 0)
        {
            transform.position = s_Hits[0].pose.position;
            transform.rotation = s_Hits[0].pose.rotation;
        }
    }
}
