using UnityEngine;

public class ModeSwitch : MonoBehaviour
{
    private enum Mode
    {
        AR,
        Editor,
    }
    
    [SerializeField]
    private GameObject editorObjects;
    
    [SerializeField]
    private GameObject arObjects;
    
    // Start is called before the first frame update
    void Awake()
    {
#if UNITY_EDITOR
        ChangeMode(Mode.Editor);
#else
        ChangeMode(Mode.AR);
#endif
    }

    void ChangeMode(Mode mode)
    {
        switch (mode)
        {
            case Mode.AR:
            {
                editorObjects.SetActive(false);
                arObjects.SetActive(true);
                break;
            }
            case Mode.Editor:
            {
                arObjects.SetActive(false);
                editorObjects.SetActive(true);
                break;
            }
        }
    }
}
