using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpatialAnchorSharingConfig", menuName = "Azure Spatial Anchors/Sharing Configuration")]
public class SpatialAnchorSharingConfig : ScriptableObject
{
    [Tooltip("The base URL for the anchor sharing service.")]
    public string BaseSharingURL = "";
}
