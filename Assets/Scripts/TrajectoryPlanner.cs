using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RosMessageTypes.Geometry;
using RosMessageTypes.NiryoMoveit;
using Unity.Robotics.PickAndPlace;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.ROSTCPConnector.ROSGeometry;
using UnityEngine;

public class TrajectoryPlanner : MonoBehaviour
{
    // Hardcoded variables
    const int k_NumRobotJoints = 6;
    const float k_JointAssignmentWait = 0.1f;
    const float k_PoseAssignmentWait = 0.5f;

    // Variables required for ROS communication
    [SerializeField]
    string m_RosServiceName = "niryo_moveit";
    public string RosServiceName { get => m_RosServiceName; set => m_RosServiceName = value; }

    [SerializeField]
    GameObject m_NiryoOne;
    public GameObject NiryoOne { get => m_NiryoOne; set => m_NiryoOne = value; }
    [SerializeField]
    Transform m_Target;

    [SerializeField]
    Transform m_TargetPlacement;

    // Assures that the gripper is always positioned above the m_Target cube before grasping.
    readonly Quaternion m_PickOrientation = Quaternion.Euler(90, 90, 0);
    [SerializeField]
    Vector3 pickPoseOffset = Vector3.up * 0.01f;

    [SerializeField]
    Vector3 localGraspPoseOffset = new Vector3(0.009f, 0.1f, 0);

    [SerializeField]
    Vector3 localPlacePoseOffset = new Vector3(0.022f, 0.1f, -0.054f);

    // Articulation Bodies
    ArticulationBody[] m_JointArticulationBodies;
    ArticulationBody m_LeftGripper;
    ArticulationBody m_RightGripper;


    // ROS Connector
    ROSConnection m_Ros;

    private int targetPlacementIndex;
    private Transform targetPrefab;
    private Vector3 targetPosition;
    private Quaternion targetRotation;
    private Transform currentTarget;
    private Transform gripperBase;
    private bool undo;

    [SerializeField]
    private float leftGripperDrive = 0.0f;
    [SerializeField]
    private float rightGripperDrive = 0.0f;

    /// <summary>
    ///     Find all robot joints in Awake() and add them to the jointArticulationBodies array.
    ///     Find left and right finger joints and assign them to their respective articulation body objects.
    /// </summary>
    void Start()
    {
        // Get ROS connection static instance
        m_Ros = ROSConnection.GetOrCreateInstance();
        m_Ros.RegisterRosService<MoverServiceRequest, MoverServiceResponse>(m_RosServiceName);

        m_JointArticulationBodies = new ArticulationBody[k_NumRobotJoints];

        var linkName = string.Empty;
        for (var i = 0; i < k_NumRobotJoints; i++)
        {
            linkName += SourceDestinationPublisher.LinkNames[i];
            m_JointArticulationBodies[i] = m_NiryoOne.transform.Find(linkName).GetComponent<ArticulationBody>();
        }


        // Find left and right fingers
        var rightGripper = linkName + "/tool_link/gripper_base/servo_head/control_rod_right/right_gripper";
        var leftGripper = linkName + "/tool_link/gripper_base/servo_head/control_rod_left/left_gripper";
        var gripperBaseName = linkName + "/tool_link/gripper_base/";

        m_RightGripper = m_NiryoOne.transform.Find(rightGripper).GetComponent<ArticulationBody>();
        m_LeftGripper = m_NiryoOne.transform.Find(leftGripper).GetComponent<ArticulationBody>();
        gripperBase = m_NiryoOne.transform.Find(gripperBaseName);

        targetPrefab = m_Target.GetChild(0);
        targetPosition = targetPrefab.localPosition;
        targetRotation = targetPrefab.localRotation;

        for (int i = m_Target.childCount; i < m_TargetPlacement.childCount; i++)
        {
            var newTarget = GameObject.Instantiate(targetPrefab, m_Target);
            newTarget.localPosition = targetPosition;
            newTarget.localRotation = targetRotation;
            newTarget.gameObject.SetActive(false);
        }

        undo = false;
    }

    /// <summary>
    ///     Close the gripper
    /// </summary>
    void CloseGripper()
    {
        var leftDrive = m_LeftGripper.xDrive;
        var rightDrive = m_RightGripper.xDrive;

        leftDrive.target = -0.001f;
        rightDrive.target = 0.001f;

        m_LeftGripper.xDrive = leftDrive;
        m_RightGripper.xDrive = rightDrive;

        
    }

    /// <summary>
    ///     Open the gripper
    /// </summary>
    void OpenGripper()
    {
        var leftDrive = m_LeftGripper.xDrive;
        var rightDrive = m_RightGripper.xDrive;

        leftDrive.target = 0.005f;
        rightDrive.target = -0.005f;

        m_LeftGripper.xDrive = leftDrive;
        m_RightGripper.xDrive = rightDrive;

        
    }

    public void SetGripper()
    {
        var leftDrive = m_LeftGripper.xDrive;
        var rightDrive = m_RightGripper.xDrive;

        leftDrive.target = leftGripperDrive;
        rightDrive.target = rightGripperDrive;

        m_LeftGripper.xDrive = leftDrive;
        m_RightGripper.xDrive = rightDrive;
        
    }

    void BindToGripper(bool bind)
    {
        if (bind)
        {
            currentTarget.SetParent(gripperBase);
            if (currentTarget.TryGetComponent(out Rigidbody rigibody))
            {
                rigibody.isKinematic = true;
            }
        }
        else
        {
            currentTarget.SetParent(m_Target);
            if (currentTarget.TryGetComponent(out Rigidbody rigibody))
            {
                rigibody.isKinematic = false;
            }
        }
    }

    /// <summary>
    ///     Get the current values of the robot's joint angles.
    /// </summary>
    /// <returns>NiryoMoveitJoints</returns>
    NiryoMoveitJointsMsg CurrentJointConfig()
    {
        var joints = new NiryoMoveitJointsMsg();

        for (var i = 0; i < k_NumRobotJoints; i++)
        {
            joints.joints[i] = m_JointArticulationBodies[i].jointPosition[0];
        }

        return joints;
    }

    public void PublishNextTask()
    {
        undo = false;

        var target = m_Target.GetChild(0);
        var targetPlacement = m_TargetPlacement.GetChild(targetPlacementIndex);
        PublishJoints(target, targetPlacement);
    }

    public void PublishPreviousTask()
    {
        undo = true;

        var target = m_Target.GetChild(m_Target.childCount - 1);
        var targetPlacement = m_Target.GetChild(0);
        targetPlacement.gameObject.SetActive(false);
        PublishJoints(target, targetPlacement);

        if (targetPlacementIndex == 0) targetPlacementIndex = m_TargetPlacement.childCount - 1;
    }

    /// <summary>
    ///     Create a new MoverServiceRequest with the current values of the robot's joint angles,
    ///     the target cube's current position and rotation, and the targetPlacement position and rotation.
    ///     Call the MoverService using the ROSConnection and if a trajectory is successfully planned,
    ///     execute the trajectories in a coroutine.
    /// </summary>
    public void PublishJoints(Transform target, Transform targetPlacement)
    {
        if (targetPlacement.TryGetComponent(out TargetPlacement targetPlacementComponent))
        {
            targetPlacementComponent.Target = target.gameObject;
        }

        currentTarget = target;

        // PreGrasp -> Grasp -> PickUp
        var graspPoseOffset = target.localRotation * localGraspPoseOffset;

        var graspPosition = target.localPosition + graspPoseOffset;
        var pickPosition = graspPosition + pickPoseOffset;

        // Approach -> Place -> Retract
        var placeOffset = targetPlacement.localRotation * localPlacePoseOffset;

        var placePosition = targetPlacement.localPosition + placeOffset;
        var approachPosition = placePosition + pickPoseOffset;

        var request = new MoverServiceRequest
        {
            joints_input = CurrentJointConfig(),
            // Pick Pose
            pick_pose = new PoseMsg
            {
                position = pickPosition.To<FLU>(),

                // The hardcoded x/z angles assure that the gripper is always positioned above the target cube before grasping.
                orientation = Quaternion.Euler(90, target.eulerAngles.y + 90, 0).To<FLU>()
            },
            // Grashp Pose
            grasp_pose = new PoseMsg
            {
                position = graspPosition.To<FLU>(),

                // The hardcoded x/z angles assure that the gripper is always positioned above the target cube before grasping.
                orientation = Quaternion.Euler(90, target.eulerAngles.y + 90, 0).To<FLU>()
            },
            // Approach Pose
            approach_pose = new PoseMsg
            {
                position = approachPosition.To<FLU>(),

                // The hardcoded x/z angles assure that the gripper is always positioned above the target cube before grasping.
                orientation = Quaternion.Euler(90, targetPlacement.eulerAngles.y + 90, 0).To<FLU>()
            },
            // Place Pose
            place_pose = new PoseMsg
            {
                position = placePosition.To<FLU>(),
                orientation = Quaternion.Euler(90, targetPlacement.eulerAngles.y + 90, 0).To<FLU>()
            }
        };

        m_Ros.SendServiceMessage<MoverServiceResponse>(m_RosServiceName, request, TrajectoryResponse);
    }

    void TrajectoryResponse(MoverServiceResponse response)
    {
        if (response.trajectories.Length > 0)
        {
            Debug.Log("Trajectory returned.");
            StartCoroutine(ExecuteTrajectories(response));
            if(!undo)
            {
                targetPlacementIndex++;
                if (targetPlacementIndex > m_TargetPlacement.childCount)
                    targetPlacementIndex = 0;
            }
            else
            {
                currentTarget.SetAsFirstSibling();
                targetPlacementIndex--;
                if (targetPlacementIndex < 0)
                    targetPlacementIndex = m_TargetPlacement.childCount;
            }
        }
        else
        {
            Debug.LogWarning("No trajectory returned from MoverService.");
        }
    }

    /// <summary>
    ///     Execute the returned trajectories from the MoverService.
    ///     The expectation is that the MoverService will return four trajectory plans,
    ///     PreGrasp, Grasp, PickUp, and Place,
    ///     where each plan is an array of robot poses. A robot pose is the joint angle values
    ///     of the six robot joints.
    ///     Executing a single trajectory will iterate through every robot pose in the array while updating the
    ///     joint values on the robot.
    /// </summary>
    /// <param name="response"> MoverServiceResponse received from niryo_moveit mover service running in ROS</param>
    /// <returns></returns>
    IEnumerator ExecuteTrajectories(MoverServiceResponse response)
    {
        if (response.trajectories != null)
        {
            // For every trajectory plan returned
            for (var poseIndex = 0; poseIndex < response.trajectories.Length; poseIndex++)
            {
                // For every robot pose in trajectory plan
                foreach (var t in response.trajectories[poseIndex].joint_trajectory.points)
                {
                    var jointPositions = t.positions;
                    var result = jointPositions.Select(r => (float)r * Mathf.Rad2Deg).ToArray();

                    // Set the joint values for every joint
                    for (var joint = 0; joint < m_JointArticulationBodies.Length; joint++)
                    {
                        var joint1XDrive = m_JointArticulationBodies[joint].xDrive;

                        joint1XDrive.target = result[joint];
                        m_JointArticulationBodies[joint].xDrive = joint1XDrive;
                    }

                    // Wait for robot to achieve pose for all joint assignments
                    yield return new WaitForSeconds(k_JointAssignmentWait);
                }

                // Close the gripper if completed executing the trajectory for the Grasp pose
                switch (poseIndex)
                {
                    case (int)Poses.PreGrasp:
                        {
                            Debug.Log($"PreGrasp pose: {poseIndex}/{response.trajectories.Length}");
                            OpenGripper();
                            break;
                        }

                    case (int)Poses.Grasp:
                        {
                            Debug.Log($"Grasp pose: {poseIndex}/{response.trajectories.Length}");
                            CloseGripper();
                            yield return new WaitForSeconds(k_PoseAssignmentWait);
                            BindToGripper(true);
                            break;
                        }
                    case (int)Poses.Place:
                        {
                            //open the gripper to place the target cube
                            Debug.Log($"Place pose: {poseIndex}/{response.trajectories.Length}");
                            OpenGripper();
                            yield return new WaitForSeconds(k_PoseAssignmentWait*2);
                            BindToGripper(false);
                            break;
                        }
                }

                // Wait for the robot to achieve the final pose from joint assignment
                yield return new WaitForSeconds(k_PoseAssignmentWait);
            }

        }

        m_Target.GetChild(0).gameObject.SetActive(true);
    }

    enum Poses
    {
        PreGrasp,
        Grasp,
        PickUp,
        Approach,
        Place,
        Retract
    }
}
